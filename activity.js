db.fruits.aggregate([
	{$match: {onSale: true} },
	{$group: {_id: "onSale: true", fruitsOnSale: {$sum: 1} } },
	{$project: {_id: 0}}
]);

db.fruits.aggregate([
	{$match: {stock: {$gte: 20} } },
	{$group: {_id: "stock", enoughStock: {$sum: 1} } },
	{$project: {_id: 0}}
]);

db.fruits.aggregate([
	{$match: {onSale: true} },
	{$group: {_id: "$supplier_id", avg_price: { $avg: { $multiply: ["$price", "$supplier_id"] }}}}
]);

db.fruits.aggregate([
	{$group: {_id: "$supplier_id", max_price: { $max: { $multiply: ["$price", 1] }}}}
]);

db.fruits.aggregate([
	{$group: {_id: "$supplier_id", min_price: { $min: "$price" }}},
	{ $sort : { min_price : 1 } }
]);



